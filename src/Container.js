import React from 'react';
import Table from './Table';


class Container extends React.Component {
    state = {
        info : [
            {
                id: 1,
                name:"Ahmed",
                age: "25"
            },
            {
                
                id: 2,
                name:"sara",
                age: "23",

            },
            {
                id:3,
                name:"omnia",
                age:"21"
            },
        ]
    }

    render() {
        return (
            <div className="container">
                <Table props={this.state.info}/>
            </div>
        )
        
    }
}

export default Container;