import React from "react";
import TableRow from "./TableRow";


let Table = ({props}) => {
    return(
        <table className="table table-bordered">
            <thead>
                <tr>
                    <th>id</th>
                    <th>name</th>
                    <th>age</th>
                </tr>
            </thead>
            <tbody>
                {props.map(row => <TableRow item={row} />)}
            </tbody>
        </table>
    )
}
export default Table;