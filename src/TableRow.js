import React from 'react';


let TableRow = ({item}) => {

    return (
        <tr>
            <td>{item.id}</td>
            <td>{item.name}</td>
            <td>{item.age}</td>
        </tr>
    )

}
export default TableRow;
